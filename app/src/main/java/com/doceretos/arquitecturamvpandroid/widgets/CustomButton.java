package com.doceretos.arquitecturamvpandroid.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.doceretos.arquitecturamvpandroid.R;


public class CustomButton extends AppCompatButton {
    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StyleButton);
        String fontName = typedArray.getString(R.styleable.StyleButton_buttonFont);
        setFontStyLe(fontName);

    }

    public void setFontStyLe(String nameFont) {
        String pathFont = "";
        switch (nameFont) {
            case "1":
                pathFont = "fonts/source-sans-pro.bold.ttf";
                break;
            case "2":
                pathFont = "fonts/source-sans-pro.regular.ttf";
                break;
            case "3":
                pathFont = "fonts/source-sans-pro.semibold.ttf";
        }
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), pathFont);
        setTypeFace(typeface, this);
    }
    public static void setTypeFace(Typeface typeFace, View view) {
        if (view instanceof Button) {
            ((TextView) view).setTypeface(typeFace);
        } else if (view instanceof EditText) {
            ((TextView) view).setTypeface(typeFace);
        } else if (view instanceof Button) {
            ((TextView) view).setTypeface(typeFace);
        }

    }
}
