package com.doceretos.arquitecturamvpandroid.api;

import com.doceretos.arquitecturamvpandroid.api.model.request.LoginUserRequest;
import com.doceretos.arquitecturamvpandroid.core.util.content.Constants;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserEndPoint {

    @POST(Constants.USER_LOGIN)
    Observable<LoginUserRequest> attemptLogin(@Body LoginUserRequest loginUserRequest);
}
