package com.doceretos.arquitecturamvpandroid.api.model.request;

import com.google.gson.annotations.SerializedName;

public class LoginUserRequest {

	@SerializedName("EmailUsuario")
	private String emailUsuario;

	@SerializedName("Llave")
	private String llave;

	@SerializedName("DeviceId")
	private String deviceId;

	@SerializedName("Token")
	private String token;

	@SerializedName("MetodoAutenticacion")
	private String metodoAutenticacion;

	@SerializedName("Password")
	private String password;

	public LoginUserRequest(String emailUsuario, String llave, String deviceId, String token, String metodoAutenticacion, String password) {
		this.emailUsuario = emailUsuario;
		this.llave = llave;
		this.deviceId = deviceId;
		this.token = token;
		this.metodoAutenticacion = metodoAutenticacion;
		this.password = password;
	}

	public void setEmailUsuario(String emailUsuario){
		this.emailUsuario = emailUsuario;
	}

	public String getEmailUsuario(){
		return emailUsuario;
	}

	public void setLlave(String llave){
		this.llave = llave;
	}

	public String getLlave(){
		return llave;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setMetodoAutenticacion(String metodoAutenticacion){
		this.metodoAutenticacion = metodoAutenticacion;
	}

	public String getMetodoAutenticacion(){
		return metodoAutenticacion;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}
}