package com.doceretos.arquitecturamvpandroid.core.util.formatter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {
    private static final Pattern VALID_EMAIL = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean isRightEmailFormat(String email){
        Matcher matcher = VALID_EMAIL.matcher(email);
        return matcher.find();
    }

    public static boolean isEmptyString(String text){
        return text.equals("");
    }
}
