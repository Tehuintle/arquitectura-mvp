package com.doceretos.arquitecturamvpandroid.core.util.content;

public class Constants {
    private static final String PRODUCTION_URL = "http://api.redus.com.mx/api/";
    private static final String TEST_URL = "http://api-test.redus.com.mx/api/";
    private static final String LOCAL_URL = "http://192.168.2.77:10/api/";

    public static final String BASE_URL = LOCAL_URL;


    public static final String APP_PREFERENCES = "com.blueicon.redus.user";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String LOGGED_IN = "logged_in";
    public static final String USER_ID = "user_id";
    public static final int CONNECTION_TIMEOUT = 30;
    public static final String USER_LOGIN = "ServiciosUsuarioNormal/Login_Usuario";
}
