package com.doceretos.arquitecturamvpandroid.core.util.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.Window;

import com.doceretos.arquitecturamvpandroid.R;


public class LoadingDialog {

    private  ProgressDialog dialog;

    public LoadingDialog(Context context) {
        dialog = buildProgressDialog(context);
    }

    public void show(boolean isShowing){
        if(isShowing) {
            dialog.show();
        }else {
            dialog.dismiss();
        }
    }

    private ProgressDialog buildProgressDialog(Context context){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //progressDialog.setContentView(LayoutInflater.from(context).inflate(R.layout.progress_dialog, null));
        return progressDialog;
    }
}