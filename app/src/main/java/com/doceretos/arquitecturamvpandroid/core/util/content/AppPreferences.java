package com.doceretos.arquitecturamvpandroid.core.util.content;

import android.content.Context;
import android.content.SharedPreferences;

import com.doceretos.arquitecturamvpandroid.api.model.response.AccessToken;
import com.google.gson.Gson;

public class AppPreferences {

    public static int getUserId(Context context) {
        return context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE).getInt(Constants.USER_ID, 0);
    }

    public static void saveAccessToken(Context context, AccessToken accessToken){
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.ACCESS_TOKEN, new Gson().toJson(accessToken));
        editor.apply();
    }

    public static String getAccessToken(Context activity){
        return activity.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE).getString(Constants.ACCESS_TOKEN, "");
    }

    public static void refreshToken(Context context, boolean refreshToken){
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.REFRESH_TOKEN, refreshToken);
        editor.apply();
    }

    public static boolean isRefreshToken(Context context){
        return context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE).getBoolean(Constants.REFRESH_TOKEN, false);
    }

    /*public static void saveUserSession(Context context, PerfilUsuario userProfile){
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.USER_PROFILE, new Gson().toJson(userProfile));
        editor.putBoolean(Constants.LOGGED_IN, true);
        editor.apply();
    }*/

    public static boolean isLoggedIn(Context context) {
        return context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE).getBoolean(Constants.LOGGED_IN, false);
    }

    public static void logout(Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }
}
