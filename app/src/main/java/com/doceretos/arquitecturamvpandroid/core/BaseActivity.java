package com.doceretos.arquitecturamvpandroid.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        bindViews();
        initViews();
        makeLading();
        setupInjection();
    }

    protected abstract int getLayoutId();

    public void bindViews() {
        ButterKnife.bind(this);
    }

    public void initViews(){

    }

    public void makeLading() {

    }

    public void setupInjection() {

    }

}
