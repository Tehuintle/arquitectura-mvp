package com.doceretos.arquitecturamvpandroid.core;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.doceretos.arquitecturamvpandroid.api.model.response.AccessToken;
import com.doceretos.arquitecturamvpandroid.core.util.content.AppPreferences;
import com.doceretos.arquitecturamvpandroid.core.util.content.Constants;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class CoreModule {
    private Fragment fragment;
    private Activity activity;
    private Context context;

    public CoreModule(Fragment fragment) {
        this.fragment = fragment;
        this.context = this.fragment.getActivity().getApplicationContext();
    }

    public CoreModule(Activity activity) {
        this.activity = activity;
        this.context = this.activity.getApplicationContext();
    }

    @Provides
    @Singleton
    Activity providesActivity(){
        return this.activity;
    }

    @Provides
    @Singleton
    Fragment providesFragment(){
        return this.fragment;
    }

    @Provides
    @Singleton
    public OkHttpClient providesHttpClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = null;
        try {
            String token = AppPreferences.getAccessToken(this.context);
            boolean isRefreshToken = AppPreferences.isRefreshToken(context);
            Gson gson = new Gson();
            final AccessToken accessToken = gson.fromJson(token, AccessToken.class);
            if (!token.equals("") && !isRefreshToken) {
                httpClient = new OkHttpClient.Builder().connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS).addInterceptor(interceptor).addInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException{
                                Request original = chain.request();

                                Request.Builder requestBuilder = original.newBuilder()
                                        .header("Accept", "application/json")
                                        .header("Content-type", "application/json")
                                        .header("Authorization",
                                                accessToken.getTokenType() + " " + accessToken.getAccessToken())
                                        .method(original.method(), original.body());

                                Request request = requestBuilder.build();
                                return chain.proceed(request);
                            }
                        }).build();
            } else {
                httpClient = makeSimpleHttpClient();
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return httpClient;
    }

    @Provides
    @Singleton
    public Retrofit providesRetrofit(OkHttpClient httpClient){
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private OkHttpClient makeSimpleHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS).addInterceptor(interceptor).build();
    }
}
