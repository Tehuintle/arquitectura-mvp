package com.doceretos.arquitecturamvpandroid.bussines;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.doceretos.arquitecturamvpandroid.R;
import com.doceretos.arquitecturamvpandroid.core.BaseActivity;
import com.doceretos.arquitecturamvpandroid.menucool.MainActivity;

import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.login_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @OnClick({R.id.stvOmitirLogIn, R.id.btnLogIn /*, R.id.btnLoginFacebook, R.id.stvRememberPassword, R.id.btnRegister*/})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.stvOmitirLogIn:
                skipLogin();
                break;
            case R.id.btnLogIn:
                attemptLogin();
                break;
            /*case R.id.btnLoginFacebook:
                signInWithFacebook();
                break;
            case R.id.stvRememberPassword:
                resetPassword();
                break;
            case R.id.btnRegister:
                signUp();
                break;*/
        }
    }

    private void attemptLogin() {
        startActivity(new Intent(this, MainActivity.class));
    }


    private void skipLogin() {

    }


}
