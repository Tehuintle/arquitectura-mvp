package com.doceretos.arquitecturamvpandroid.bussines;

import android.os.Bundle;

import com.doceretos.arquitecturamvpandroid.R;
import com.doceretos.arquitecturamvpandroid.core.BaseActivity;

public class MenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.menu_activity;
    }

    @Override
    public void initViews() {
        super.initViews();
    }

    @Override
    public void setupInjection() {
        super.setupInjection();
    }

    @Override
    public void makeLading() {
        super.makeLading();
    }

}
